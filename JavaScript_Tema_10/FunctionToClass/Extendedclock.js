class ExtendedClock extends Clock {
    constructor(extended) {
      super(extended);
      let { precision = 1000 } = extended;
      this.precision = precision;
    }

    start() {
      this.render();
      this.timer = setInterval(() => this.render(), this.precision);
    }
  };