function menu1() {
  document.querySelector('.menu2').classList.add('hidden-input');
}
function displaymenu() {
  document.querySelector(".menu").removeAttribute("id");
  for (let i = 0; i < document.querySelectorAll('input').length; i++) {
    document.querySelectorAll('input')[i].value = '';
  }
}

function hidemenu() {
  document.querySelector(".menu").setAttribute("id", "close");
  if (document.querySelector('.menu2').classList.value.includes('hidden-input') == true) {
    document.querySelector('.menu2').classList.remove('hidden-input');
  }
}

function character() {
  if (document.getElementById('player1').value == 'X') {
    document.getElementById('player2').value = 0;
  } else {
    document.getElementById('player2').value = 'X';
  }
}

var icon = document.getElementById("icon");

icon.onmouseover = function () {
  if (this.className === "Icon") {
    this.className = "Icon open";
  } else {
    this.className = "Icon";
  }
};

function checkcharacter() {
  let playername;
  let playercount = 2;
  if (document.querySelector('.menu2').classList.value.includes('hidden-input') == true) {
    playercount = 1;
  }
  for (let i = 0; i < playercount; i++) {
    if (document.querySelectorAll('input')[i].value == '') {
      document.querySelectorAll('input')[i].value = `Player ${i + 1}`;
      playername = document.querySelectorAll('input')[i].value
    } else {
      playername = document.querySelectorAll('input')[i].value
    }
    let character = document.querySelectorAll('select')[i].value;
    window.localStorage.setItem(`Player${i + 1}`, `${playername} with ${character}`);
  }

  if (playercount == 2) {
    location.href = "../MultiPlayer/MultiPlayer.html";
  } else {
    location.href = "../SinglePlayer/SinglePlayer.html";
  }
}