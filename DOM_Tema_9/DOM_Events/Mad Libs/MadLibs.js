function MadLib() {
  let storyDiv = document.getElementById("story");  
  let person = document.getElementById("person").value;
  let adjective = document.getElementById("adjective").value;
  let noun = document.getElementById("noun").value;
  storyDiv.innerHTML = (`${person} really likes ${adjective} ${noun}!`);
}
//Function responsible for retrieving the text from each input label and writting the story in the storyDiv

let libButton = document.getElementById('lib-button');
libButton.addEventListener('click', MadLib);
//adding an event listener for the button and making it calling the function