let books = [
    {
        title: "The Design of EveryDay Things",
        author: "Don Norman",
        alreadyRead: false
    },
    {
        title: "The Most Human Human",
        author: "Brian Christian",
        alreadyRead: true
    }];

let bookList = document.createElement("ul"); //creating a node for the ul element
for (let i = 0; i < books.length; i++) {
    let list = document.createElement("li");
    list.innerHTML = (`${books[i].title} by ${books[i].author}`);
    document.body.appendChild(list);
    bookList.appendChild(list);     
}   //Script responsible for passing through each item present, appending it to an li node and displaying it
document.body.appendChild(bookList); //script responsible for appending the ul node to the body.