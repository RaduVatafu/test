let img = document.getElementsByTagName('img')[0];  //I created a variable for the first image element
img.style.left = '0px'; //setting the images starting position to always be top-left
function catWalk() {
  let currentLeft = parseInt(img.style.left); 
  img.style.left = (currentLeft + 10) + 'px'; //incrementing the images position to make it move to the right side by 10px
  if (currentLeft > (window.innerWidth-img.width)) {
    img.style.left = '0px';
  } //if the cat image reaches the right side of the screen it will start again the animation
}
window.setInterval(catWalk, 50);  //the function is called every 50 milliseconds