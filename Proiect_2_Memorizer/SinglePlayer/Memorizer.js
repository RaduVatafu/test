const deck = document.querySelectorAll('.card');
deck.forEach(card => card.addEventListener('click', flipCard));
let gameMode;

shuffleDeck(); // randomizes the board before each game

const displayScore1 = document.querySelector('#score1');
const displayScore2 = document.querySelector('#score2');
let displayMoves1 = document.querySelector('#moves1');
let displayMoves2 = document.querySelector('#moves2');
let gameOverMsg = document.querySelector('#memoryID');

let score1 = 0;
let score2 = 0;
let moves1 = 0;
let moves2 = 0;
let isFirstCard = false;
let first, second;
let isBoardClosed = false;
let p1Turn = true;
let finalScore;


function threebyfour() {
    if (gameMode == 'single') {
        document.querySelector('.Player1').innerHTML = document.querySelector('.playername1').value;
    } else {
        document.querySelector('.Player1').innerHTML = document.querySelector('.playername1').value;
        document.querySelector('.Player2').innerHTML = document.querySelector('.playername2').value;
    }
    for (let i = 0; i < 4; i++) {
        deck[i].classList.add("hide");
    }
    for (let i = 0; i < deck.length; i++) {
        deck[i].classList.remove("flip");
        deck[i].classList.remove('cursor');
    }
    shuffleDeck();
    finalScore = 6;
    document.querySelector(".game").style.display = "flex";
    document.querySelector(".pop-up").classList.add("hide");
    document.querySelector(".button3").classList.remove("hide");
    deck.forEach(card => card.addEventListener('click', flipCard));
}


function fourbyfour() {
    if (gameMode == 'single') {
        document.querySelector('.Player1').innerHTML = document.querySelector('.playername1').value;
    } else {
        document.querySelector('.Player1').innerHTML = document.querySelector('.playername1').value;
        document.querySelector('.Player2').innerHTML = document.querySelector('.playername2').value;
    }
    for (let i = 0; i < deck.length; i++) {
        if (deck[i].classList.contains("hide")) {
            deck[i].classList.remove("hide");
        }
        deck[i].classList.remove("flip");
        deck[i].classList.remove('cursor');
    }
    shuffleDeck();
    finalScore = 8;
    document.querySelector(".game").style.display = "flex";
    document.querySelector(".pop-up").classList.add("hide");
    document.querySelector(".button3").classList.remove("hide");
    deck.forEach(card => card.addEventListener('click', flipCard));
}


/**
 * Shuffles the deck so that each game has a completely different board
 */
function shuffleDeck() {
    document.querySelector('.Player1').style.color = 'white';
    deck.forEach(card => {
        let randomIndex = Math.floor(Math.random() * 16);
        card.style.order = randomIndex;
    });

}


function flipCard() {
    if (isBoardClosed) return;
    if (this === first) return;

    this.classList.add('flip');


    if (!isFirstCard) {
        isFirstCard = true; //first click
        first = this; // 'this' = the element that has fired the event
        return
    }

    isFirstCard = false; //second click
    second = this;

    // if the second card has been chosen, check if they match
    checkMatch();

}

function checkMatch() {

    if (first.dataset.id == second.dataset.id) {
        //so cards cannot be clicked again
        first.removeEventListener('click', flipCard);
        second.removeEventListener('click', flipCard);
        first.classList.add('cursor');
        second.classList.add('cursor');


        resetBoard();
        if (gameMode == 'multi') {
            if (p1Turn) {
                score1++;
                moves1++;
                displayMoves1.textContent = moves1.toString();
                displayScore1.textContent = score1.toString();
            }
            else {
                score2++;
                moves2++;
                displayMoves2.textContent = moves2.toString();
                displayScore2.textContent = score2.toString();
            }
        } else {
            score1++;
            moves1++;
            displayMoves1.textContent = moves1.toString();
            displayScore1.textContent = score1.toString();
        }

        checkGameOver();
    }
    else {
        //if the cards are not a match then turn them over again
        isBoardClosed = true;
        setTimeout(() => {
            first.classList.remove('flip');
            second.classList.remove('flip');
            isBoardClosed = false;
            resetBoard();
        }, 1000);
        if (gameMode == 'multi') {
            if (p1Turn) {
                setTimeout(function () {
                    document.querySelector('.Player2').style.color = 'white';
                    document.querySelector('.Player1').removeAttribute('style');
                }, 1000);

                moves1++;
                displayMoves1.textContent = moves1.toString();
                p1Turn = false;
            }
            else if (!p1Turn) {
                setTimeout(function () {
                    document.querySelector('.Player1').style.color = 'white';
                    document.querySelector('.Player2').removeAttribute('style');
                }, 1000);
                moves2++;
                displayMoves2.textContent = moves2.toString();
                p1Turn = true;
            }
        } else {
            moves1++;
            displayMoves1.textContent = moves1.toString();
        }

        // p2Turn = true;
    }
}


/**
 * Prevents more than 2 cards being flipped over at once since it is against the rules
 */
function resetBoard() {
    first = null;
    second = null;
    isFirstCard = false;
    isBoardClosed = false;
}

function restartGame() {
    shuffleDeck();
}

function checkGameOver() { // game is over if either player gets 28 points
    if (gameMode == "multi") {
        if (score1 === finalScore / 2 + 1) {
            isBoardClosed = true;
            document.querySelector(".message").innerHTML = `CONGRATULATIONS PLAYER ${document.querySelector(".playername1").value}! YOU WON!`;
            document.querySelector(".message").classList.remove("hide");
            shuffleDeck();
        }
        else if (score2 === finalScore / 2 + 1) {
            isBoardClosed = true;
            document.querySelector(".message").innerHTML = `CONGRATULATIONS PLAYER ${document.querySelector(".playername1").value}! YOU WON!`;
            document.querySelector(".message").classList.remove("hide");
            shuffleDeck();
        }
    } else {
        if (score1 === finalScore) {
            document.querySelector(".message").innerHTML = `CONGRATULATIONS PLAYER ${document.querySelector(".playername1").value}! YOU WON!`;
            document.querySelector(".message").classList.remove("hide");
            shuffleDeck();
        }

    }
}
function singleplayer() {
    gameMode = 'single';
    document.querySelector(".Player2-content").classList.add("hidden");
    document.querySelector(".pop-up").classList.remove("hide");
    document.querySelector(".input2").classList.add("hide");
    document.querySelector(".game-menu").classList.add("hide");
}

function multiplayerplayer() {
    gameMode = 'multi';
    document.querySelector(".pop-up").classList.remove("hide");
    document.querySelector(".input2").classList.remove("hide");
    document.querySelector(".game-menu").classList.add("hide");
}

function closemenu() {
    document.querySelector(".pop-up").classList.add("hide");
    document.querySelector(".game-menu").classList.remove("hide");
}

