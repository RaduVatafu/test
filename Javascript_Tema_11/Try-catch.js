let json = '{"name":"John", "age": 30}';

function read(userData) {
    let user = JSON.parse(userData);
    //conditii care verifica daca obiectul citit are nume si varsta
    if ((!user.name) && (!user.age)) {
        throw new SyntaxError("Incomplete data: no name and no age");
    }
    else if (!user.name) {
        throw new SyntaxError("Incomplete data: no name");
    }
    else if (!user.age) {
        throw new SyntaxError("Incomplete data: no age");
    }
    return user;
}

//extinderea clasei Error
class ValidationError extends Error {
    constructor(param) {
        super(param);
        this.name = "ValidationError";
    }
    validateuser(param) {
        let user = JSON.parse(param);
        //conditii care verifica daca tipul de date citit este corect
        if (!(typeof user.name === "string")) {
            throw new SyntaxError("Name is not correct");
        }
        else if (!(typeof user.age === "number")) {
            throw new SyntaxError("Age is not correct");
        }
    }
}


