let books = [
    {
        title: "The Lord of the Rings",
        author: "J.R.R. Tolkien",
        alreadyRead: false
    },
    {
        title: "The Hobbit",
        author: "J.R.R. Tolkien",
        alreadyRead: true
    }];

for (let i = 0; i < books.length; i++) {
    let book = books[i];
    let bookInfo = book.title + ` by ` + book.author;
    if (book.alreadyRead) {
        console.log(`You already read ${bookInfo}.`);
    } else {
        console.log(`You still need to read ${bookInfo}.`);
    }
}