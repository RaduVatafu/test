function celsiusToFahrenheit() {
    celsiusTemperature = 30;
    fahrenheit = celsiusTemperature * (9 / 5) + 32;
    console.log(`${celsiusTemperature}°C is ${fahrenheit}°F`);
}

function fahrenheitToCelsius() {
    fahrenheitTemperature = 95;
    celsius = (fahrenheitTemperature - 32) * (5 / 9);
    console.log(`${fahrenheitTemperature}°F is ${celsius}°C`);
}

celsiusToFahrenheit();
fahrenheitToCelsius();