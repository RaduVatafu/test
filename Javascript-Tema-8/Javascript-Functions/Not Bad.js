function notBad(string) {
  not = string.indexOf('not');
  bad = string.indexOf('bad');
  if (not == -1 || bad == -1 || bad < not) console.log(string);
  else {
    console.log(`${string.slice(0, not) + 'good' + string.slice(bad + 3)}`);
  }
}

notBad('This dinner is that bad!');