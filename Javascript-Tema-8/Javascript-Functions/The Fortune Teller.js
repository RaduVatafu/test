function tellFortune(children_number, partner_name, geographic_location, job_title) {
    console.log(`You will be a ${job_title} in ${geographic_location}, and married to ${partner_name} with ${children_number} children.`);
}

tellFortune(5, "Alex", "Romania", "Software Engineer");
tellFortune(2, "Silvia", "Zimbabue", "Docotor");
tellFortune(3, "Marcela", "Botswana", "Trainer");
