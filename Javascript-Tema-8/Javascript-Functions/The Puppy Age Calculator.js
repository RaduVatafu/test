function calculateDogAge(puppy_age, human_age) {
    console.log(`Your doggie is ${puppy_age * 7} years old in human years!`);
    console.log(`You are ${human_age / 7} years old in dog years`);
}

calculateDogAge(7, 21);
calculateDogAge(9, 42);
calculateDogAge(15, 35);