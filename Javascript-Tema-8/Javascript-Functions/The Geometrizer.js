function calcCircumference(radius) {
    circumference = Math.PI * 2 * radius;
    console.log(`The circumference is ${circumference}`);
}

function calcArea(radius) {
    area = Math.pow(radius, 2) * Math.PI;
    console.log(`The area is ${area}`);
}

calcCircumference(6);
calcArea(6);    