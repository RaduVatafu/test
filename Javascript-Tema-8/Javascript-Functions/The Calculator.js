function squareNumber(number) {
    square = Math.pow(number, 2);
    console.log(`The result of squaring the number ${number} is ${square}.`);
    return square;
}

squareNumber(5);

function halfNumber(number) {
    half = number / 2;
    console.log(`Half of ${number} is ${half}.`);
    return half;
}

halfNumber(10);

function percentOf(number1, number2) {
    percent = (number1 / number2) * 100;
    console.log(`${number1} is ${percent}% of ${number2}`);
    return percent;
}

percentOf(3, 9);

function areaOfCircle(radius) {
    area = (Math.pow(radius, 2) * Math.PI).toFixed(2);
    console.log(`The area for a circle with radius ${radius} is ${area}.`);
    return area;
}

areaOfCircle(2);

function final(number) {
    let half = halfNumber(number);
    let square = squareNumber(half);
    let area = areaOfCircle(square);
    let result = percentOf(area, square);
    console.log(`We take number ${number}, half of that number is ${half}, that number squred is ${square}, area of that squared number is ${area}
    and the percentage between area ${area} and the squared number ${square} is ${result}`);
}

final(10);