let shuttle_name ="Determination";
let Shuttle_Speed = 17500; 
const Distance_to_Mars = 225000000;
const Distance_to_the_Moon = 384400;
const Miles_per_kilometer = 0.621;

let Miles_to_Mars = Distance_to_Mars * Miles_per_kilometer;
let Hours_to_Mars = Miles_to_Mars/Shuttle_Speed;
let Days_to_Mars = Hours_to_Mars/24;
alert(`${shuttle_name} will take ${Days_to_Mars} days to reach Mars.`);

let Miles_to_Moon = Distance_to_the_Moon * Miles_per_kilometer;
let Hours_to_Moon = Miles_to_Moon/Shuttle_Speed;
let Days_to_Moon = Hours_to_Moon/24;
alert(`${shuttle_name} will take ${Days_to_Moon} days to reach Moon.`);